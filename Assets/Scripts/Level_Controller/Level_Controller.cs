﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Level_Controller : MonoBehaviour
{
    
    public GameObject[] Switch;
    public GameObject  Door;
    public GameObject FireWall1;
    public GameObject FireWall2;
    public GameObject WaterWall;

    private void Update()
    {
        if(Switch[0].GetComponent<Obstacle_collision>().ON_OFF==true)
        {
            //door is open 
           //Debug.Log("door is open");
            Door.SetActive(false);
        }
        else if(Switch[0].gameObject.GetComponent<Obstacle_collision>().ON_OFF==false)
        {
            //door is closed
            //Debug.Log("door is closed");
            Door.SetActive(true);
        }
        if (Switch[1].gameObject.GetComponent<Obstacle_collision>().ON_OFF == true)
        {
            //wall is on
           //Debug.Log("wall is off");
            FireWall1.SetActive(false);
        }
        else if (Switch[1].gameObject.GetComponent<Obstacle_collision>().ON_OFF == false)
        {
            //wall is off
            //Debug.Log("wall is on");
            FireWall1.SetActive(true);
        }
        if (Switch[2].gameObject.GetComponent<Obstacle_collision>().ON_OFF == true)
        {
            //wall is on
            //Debug.Log("wall is off");
            FireWall2.SetActive(false);
        }
        else if (Switch[2].gameObject.GetComponent<Obstacle_collision>().ON_OFF == false)
        {
            //wall is off
            //Debug.Log("wall is off");
            FireWall2.SetActive(true);
        }

        if(Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

    }

}
