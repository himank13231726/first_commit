﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_movement : MonoBehaviour
{
    public Vector3 EnemyPositon;
    public GameObject player;
    public float MoveSpeed;
    public float MaxDist;
    public float Mindist;
    // Start is called before the first frame update
    void Start()
    {
        EnemyPositon = gameObject.transform.position;

    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.LookAt(player.transform.position);

        if (Vector3.Distance(transform.position, player.transform.position) >= Mindist)
        {

            //Debug.Log(Vector3.Distance(transform.position, player.transform.position));   

            if (Vector3.Distance(transform.position, player.transform.position) <= MaxDist)
            {
                transform.position += transform.forward * MoveSpeed * Time.deltaTime;
            }
        }
    }
}