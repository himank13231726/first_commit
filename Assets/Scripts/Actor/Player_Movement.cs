﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The movement for player
//The script to make the motion mechanism for the player 

public class Player_Movement : MonoBehaviour
{
    //The movement for player
    GameObject player;
    Rigidbody player_body;
    Vector3 Player_position;
    private void Start()
    {
        player = GameObject.Find("Player");
        player_body = gameObject.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Player_position = player.transform.position;

        if (Input.GetKey(KeyCode.A))
        {
            Player_position.x = ((player.transform.position.x) - .10f);
            player.transform.position = Player_position;
        }
        if(Input.GetKey(KeyCode.D))
        {
            Player_position.x = ((player.transform.position.x) + .10f);
            player.transform.position = Player_position;
        }
        if(Input.GetKey(KeyCode.W))
        {
            player_body.AddForce(new Vector3(0f,.1f,0f),ForceMode.Impulse);
        }
        if(Input.GetKey(KeyCode.S))
        {

        }

    }
}
