﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//This class is to use the properties and perform actions.
//to create various instance of the 
public class Enemy : MonoBehaviour
{
    public GameObject enemy;
    public GameObject player;
    int count = 0;
    GameObject Targetplayer;
    Vector3 TargetPlayer_CurrentPosition;
    Vector3 distance_interv;
    Vector3 fixed_dist = new Vector3(10, 0, 0);

    string flag ="left";
    Vector3 enemyCurrentPosition;
    private void Update()
    {

        
        enemyCurrentPosition = enemy.transform.position;

        //to check if the player is near the enemy or not
        
            Targetplayer = player;
            TargetPlayer_CurrentPosition = Targetplayer.transform.position;
            distance_interv = enemyCurrentPosition - TargetPlayer_CurrentPosition;




        if (distance_interv.x <= fixed_dist.x || distance_interv.x >= -fixed_dist.x)
        {
            // collect the enemy from the unity  
            if (enemy == null)
            {
                Debug.Log("There are no enemies found in the scene");
            }
            else
            {
                if (flag == "left")
                {
                    enemyCurrentPosition.x = enemy.transform.position.x - 0.1f;
                    enemy.transform.position = enemyCurrentPosition;
                    count = count + 1;
                    if (count == 20)
                    {
                        flag = "right";
                        count = 0;
                    }
                }
                else if (flag == "right")
                {
                    enemyCurrentPosition.x = enemy.transform.position.x + 0.1f;
                    enemy.transform.position = enemyCurrentPosition;
                    count = count + 1;
                    if (count == 20)
                    {
                        flag = "left";
                        count = 0;
                    }
                }

            }

        }
    }
    
    



}
