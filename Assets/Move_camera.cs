﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move_camera : MonoBehaviour
{
    Vector3 Player_Position;
    public GameObject target;
        Vector3 Camera_position;
    public float distance_of_camera = -52.40f;
    
    private void Update()
    {
        Player_Position=target.transform.position;

        Camera_position.x = Player_Position.x;
        Camera_position.y=Player_Position.y;
        Camera_position.z = distance_of_camera;
        gameObject.transform.position = Camera_position;
        
    }
    
}
