﻿using UnityEngine;
using UnityEditor;
/// <summary>
/// Author:         Mark Mendoza
/// 
/// Description:    Editor window tool for level creation, speeding up the pipeline
///                 for multiple levels.
///                 The tool takes a png image and reads its pixel co-ordinance(color and position) which
///                 correlates to a specific GameObject prefab(defined by the user) and position to be spawned in the scene view.
///                 
/// Date:           04/09/2019
/// 
/// Notes:          PNG setup when importing into asset folder:
///                 1 - Enable read and writing in advanced dropdown
///                 2 - Change compression to none.
///                 This ensures that the colors retain their value when being imported
/// </summary>
public class LevelGeneratorEditor : EditorWindow
{
    [SerializeField]
    private string levelName = "Level_000";
    [SerializeField]
    private GameObject levelHolder;
    [SerializeField]
    private Texture2D pngMapToScan;
    [SerializeField]
    private ColorToObject[] colorToObject;

    ScriptableObject target;
    SerializedObject serializedObject;
    SerializedProperty colorCode;

    Vector2 scrollPos;

    [MenuItem("LevelGenerator/ScanPNG")]
    static void StartWindow()
    {
        LevelGeneratorEditor levelGeneratorEditor = GetWindow<LevelGeneratorEditor>();
        levelGeneratorEditor.Show();
    }

    private void OnGUI()
    {
        //Dynamic scroll bar appears when window can't contain all the input fields.
        scrollPos = GUILayout.BeginScrollView(scrollPos, false, false);
        GUILayout.BeginVertical();
        //Title
        GUILayout.Space(8);
        GUI.skin.label.fontSize = 16;
        GUI.skin.label.alignment = TextAnchor.MiddleCenter;
        GUILayout.Label("Png To Level Generator");
        GUILayout.Space(32);
        //Input field for name of level.
        levelName = EditorGUILayout.TextField("Level Name", levelName);
        GUILayout.Space(8);
        //Input field for png Texture
        pngMapToScan = (Texture2D)EditorGUILayout.ObjectField("Level Layout", pngMapToScan, typeof(Texture2D), false);
        GUILayout.Space(8);
        //Input field for color codes
        EditorGUILayout.PropertyField(colorCode, true);
        serializedObject.ApplyModifiedProperties();
        GUILayout.Space(30);
        //Button to generate level
        if (GUILayout.Button("Generate Level"))
        {
            GenerateLevel(pngMapToScan);
        }
        GUILayout.EndVertical();
        GUILayout.EndScrollView();
    }
    /// <summary>
    /// Initialise serialised objects and properties and load a previous state if used before.
    /// </summary>
    private void OnEnable()
    {
        //Load saved state of the window if it exist.
        string savedItems = EditorPrefs.GetString("pngToLevel", JsonUtility.ToJson(this, false));
        JsonUtility.FromJsonOverwrite(savedItems, this);

        target = this;
        serializedObject = new SerializedObject(target);
        colorCode = serializedObject.FindProperty("colorToObject");
    }
    /// <summary>
    /// Save the input fields of the window when closing.
    /// This ensures that when reopening the window, the values are not reset.
    /// </summary>
    private void OnDisable()
    {
        string savedItems = JsonUtility.ToJson(this, false);
        EditorPrefs.SetString("pngToLevel", savedItems);
    }
    /// <summary>
    /// Generates a level by reading each pixel color of the image and using that information 
    /// to set the position of a defined prefab that will spawn into the scene.
    /// 
    /// Arguments:
    ///     -PNG(preferably) image to be read. The image must not be compressed and allows to be read/written to.
    /// </summary>           
    private void GenerateLevel(Texture2D map)
    {
        //Create empty parent GameObject to hold the level
        levelHolder = new GameObject(levelName);
        Color pixelColor;
        Vector2 spawnLocation;
        //Iterate through each pixel of the texture map to read its color.
        for (int y = 0; y < map.height; y++)
        {
            for (int x = 0; x < map.width; x++)
            {
                pixelColor = map.GetPixel(x, y);
                //Continue loop if transparent color.
                if (pixelColor.a == 0)
                {
                    continue;
                }
                //Iterate through the colorToObject array and see if the pixel color matches the color stored in the array.
                for (int i = 0; i < colorToObject.Length; i++)
                {
                    if (pixelColor.Equals(colorToObject[i].color))
                    {
                        spawnLocation = new Vector2(x, y);
                        //Instantiate object and set its parent to the empty GameObject.
                        (Instantiate(colorToObject[i].prefab, spawnLocation, Quaternion.identity) as GameObject).transform.parent = levelHolder.transform;
                    }
                }
            }
        }
    }
    /// <summary>
    /// Class data that stores a color to match a prefab GameObject.
    /// </summary>
    [System.Serializable]
    public class ColorToObject
    {
        public Color color;
        public GameObject prefab;
    }
}